﻿$(function () {
    $("#slides-home").responsiveSlides({
        auto: true,
        pager: true,
        nav: false,
        pause: false,
        speed: 500,
        timeout: 5000,
        namespace: "transparent-btns"
    });

    var addBtns = $(".add");
    var subBtns = $(".sub");
    var numbers = $(".number")
    var i;
    for(i=0; i<addBtns.length;i++){
        addBtns[i].on("click", function(){
            numbers[i].val() = numbers[i].val()+1;
        });
    }
});
